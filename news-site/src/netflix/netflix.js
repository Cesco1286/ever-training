import React, { Component } from 'react';
import './netflix.css';

var imgs = [
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://lumiere-a.akamaihd.net/v1/images/r_blackpanther_nowonhero_e6434d76.jpeg?region=0,0,2048,832",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
  "https://rgermany70.files.wordpress.com/2013/03/the_avengers___wallpaper_by_capthesupersoldier-d5dvy483.jpg",
];


class Netflix extends Component {
  mapImgs(){
    return imgs.map(elem=> {
      return <li className="movie">
              <img src={elem}/>
            </li>;
    })
  }

  render() {
    return (
      <div>
        <div className="nf-navbar">          
          <ul>
            <a href="www.google.it">
             <img href="http://www.netflix.com" src="http://www.stickpng.com/assets/images/580b57fcd9996e24bc43c529.png"></img>
            </a>
          </ul>
        </div>
        <div className="netflix-container">        
          <div className="movie-genre">
            <h4>
              Action
            </h4>
          </div>
          <div className="movie-category">                                  
              <ul>
                {this.mapImgs()}
              </ul>
          </div> 
        <div className="movies-container-flex">
          <div className="movie-flex">            
            <h4>The avengers</h4>
            <img src={imgs[0]}/>
          </div>
          <div className="movie-flex">
            <img src={imgs[1]}/>
          </div>
        </div>
        <footer> 
          <h4>Fakeflix info:</h4>
          <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec massa vehicula, dapibus urna at, tristique mauris. Nullam ultrices convallis facilisis. Donec nec erat eu lacus sagittis accumsan. Curabitur iaculis tempor neque, sed lobortis risus dictum in. Sed viverra a elit non maximus. Quisque feugiat nunc eget arcu sagittis, sed maximus erat aliquet. Curabitur tincidunt ultricies neque a posuere. Nullam elementum lacinia leo et porta. Sed nulla diam</h6>
        </footer>
        
        </div>
      
      </div>
    );
  }
}

export default Netflix;
